package mundo;

public class Computador extends Electronico
{
	public enum SistemaOperativo
	{
		WINDOWS, MAC, LINUX, NINGUNO
	}
	private SistemaOperativo sistemaOperativo;
	public Computador(Gama pGama, double pPrecio, SistemaOperativo pSistemaOperativo) {
		super(pGama, pPrecio);
		sistemaOperativo=pSistemaOperativo;
	}

	public String toString()
	{
		return "Computador "+sistemaOperativo.name()+" - "+"Gama "+gama;
	}
}
