package mundo;

import javax.lang.model.element.Element;

public class Casillero<T> {

	private T elemento;

	public Casillero ()
	{
		elemento = null;
	}

	public boolean agregar(T pt)
	{
		if(elemento == null)
		{
			elemento = pt;
			return true;
		}
		return false;
	}

	public T darProducto()
	{
		return elemento;
	}

	public T despachar()
	{
		if(elemento==null)
		{
			return elemento;
		}
		
		T nObjeto = elemento;
		elemento = null;
		return nObjeto;
	}

	public boolean estaDesocupado() {
		
		if(elemento==null)
		{
			return true;
		}
		return false;
		
	}
}
