package taller.mundo;

import taller.estructuras.Heap;

public class Pizzeria 
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------
	
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	private Heap<Pedido> heapRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO 
 	/** 
	 * Heap de elementos por despachar
	 */
	private Heap<Pedido> heapDespachos;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO 
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		heapRecibidos=new Heap<>(false);
		heapRecibidos=new Heap<>(true);
		
	}
	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		Pedido nuevo= new Pedido(precio, nombreAutor, cercania);
		nuevo.setOrdenado(false);
		heapRecibidos.add(nuevo);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		Pedido  n = heapRecibidos.poll();
		if(n!=null)
		{
			n.setOrdenado(true);
			heapDespachos.add(n);
		}
		return n;
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
	    return  heapDespachos.poll();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        return heapRecibidos.getOrderQuery();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
    	 Pedido[] lista= new Pedido[heapRecibidos.size()];
         for(int i =0;i<lista.length;i++)
         	lista[i]=heapRecibidos.poll();
         return lista;
     }
}
