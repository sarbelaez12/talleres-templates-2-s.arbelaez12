package taller.mundo;

public class Pedido implements Comparable<Pedido>
{

	// ----------------------------------
	// Atributos
	// ----------------------------------

	/**
	 * Precio del pedido
	 */
	private double precio;
	
	/**
	 * Autor del pedido
	 */
	private String autorPedido;
	
	/**
	 * Cercania del pedido
	 */
	private int cercania;
	
	private Boolean ordenadoPorCercania;
	
	// ----------------------------------
	// Constructor
	// ----------------------------------
	
	public Pedido(double pPrecio, String pAutor, int pCercania)
	{
		precio=pPrecio;
		autorPedido=pAutor;
		cercania=pCercania;
	}
	
	// ----------------------------------
	// Métodos
	// ----------------------------------
	
	/**
	 * Getter del precio del pedido
	 */
	public double getPrecio()
	{
		return precio;
	}
	
	/**
	 * Getter del autor del pedido
	 */
	public String getAutorPedido()
	{
		return autorPedido;
	}
	
	/**
	 * Getter de la cercania del pedido
	 */
	public int getCercania() {
		return cercania;
	}
	@Override
	public int compareTo(Pedido o) {
		if(ordenadoPorCercania)
			return cercania- o.getCercania();
		else
			return (int) ((precio==o.getPrecio())?0:(precio-o.getPrecio())/(precio-o.getPrecio()));
	}
	public void setOrdenado(Boolean EsOrdenadoPorercania){ordenadoPorCercania=EsOrdenadoPorercania;}
}
