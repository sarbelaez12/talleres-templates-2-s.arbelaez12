package taller.estructuras;

import java.util.ArrayList;

public class Heap<T extends Comparable<T>> implements IHeap<T>{
	private Comparable<T>[] lista;
	private boolean esAscendente;
	private int ultimo;
	private int tamanio;
	public Heap(Boolean pEsascendente)
	{
		esAscendente=pEsascendente;
		tamanio=1;
		lista =new Comparable[tamanio];
		ultimo=0;
		
	}
	//Metodos internos
	private int parent(int j) {return (j-1)/2;}
	private int left(int j) {return 2*j+1;}
	private int right(int j) {return 2*j+2;}
	private boolean hasLeft(int j){return left(j)<ultimo;}
	private boolean hasRight(int j){return right(j)<ultimo;}
	
	private void swap(int i, int j)
	{
		Comparable<T> temp= lista[i];
		lista[i]=lista[j];
		lista[j]=temp;
	}
	
	@Override
	public void add(T elemento) {
		ultimo++;
		if(ultimo >= tamanio) resize();
		lista[ultimo-1]=elemento;
		if(ultimo==1)
			return;
		siftUp();
		
	}

	private void resize() {
		tamanio*=2;
		Comparable<T>[] n = new Comparable[tamanio];
		int contador=0;
		for(Comparable actual:lista)
		{
			n[contador]=actual;
			contador++;
		}
		lista=n;
		
	}
	@Override
	public T peek() {
		return (T) lista[0];
	}

	@Override
	public T poll() {
		if(ultimo==0) return null;
		Comparable<T> buscado= lista[0];
		lista[0]=lista[ultimo-1];
		lista[ultimo-1]=null;
		ultimo--;
		siftDown();
		return (T)buscado;
		}
	@Override
	public int size() {
		return tamanio;
	}

	@Override
	public boolean isEmpty() {
		return (ultimo==0);
	}

	@Override
	public void siftUp() {
		int j= ultimo-1;
		while(j>0)
		{
			int p = parent(j);
			if(esAscendente)
			{
				if(lista[j].compareTo((T) lista[p])<0)
				{
					swap(j,p);
					j=p;
				}
				else
				{
					break;
				}
			}
			else
			{
				if(lista[j].compareTo((T) lista[p])>0)
				{
					swap(j,p);
					j=p;
				}
				else
				{
					break;
				}
			}
		}
		
	}

	@Override
	public void siftDown() {
		int j=0;
		while(hasLeft(j))
		{
			int hijoACambiar=left(j);
			if(hasRight(j))
			{
				if(esAscendente)
					hijoACambiar=(lista[hijoACambiar].compareTo((T) lista[right(j)])>0)?left(j):right(j);
				else
				{
					hijoACambiar=(lista[hijoACambiar].compareTo((T) lista[right(j)])<0)?left(j):right(j);	
				}
				
			}
			if(esAscendente)
			{
				if(lista[j].compareTo((T) lista[hijoACambiar])>0)
				{
					swap(j, hijoACambiar);
					j=hijoACambiar;
				}
				else
				{
					break;
				}
			}else
			{
				if(lista[j].compareTo((T) lista[hijoACambiar])<0)
				{
					swap(j, hijoACambiar);
					j=hijoACambiar;
				}
				else
				{
					break;
				}
			}
		}
		
		
	}
	public T[] getOrderQuery()
	{
		Comparable[] n = new Comparable[ultimo];
		for(int i =0;i<ultimo;i++)
		{
			n[i]=poll();
		}
		for(int i =0;i<ultimo;i++)
		{
			add((T) n[i]);
		}
		return (T[]) n;
	}
}
