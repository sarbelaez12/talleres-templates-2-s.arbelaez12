package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import estructuras.ListaSimplementeEncadenada.Node;
import modelos.Paradero;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private ListaSimplementeEncadenada<T> listaNodos;

	/**
	 * Lista de adyacencia 
	 */
	private HashTable<Integer, ListaSimplementeEncadenada<Arco>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		adj= new HashTable<Integer, ListaSimplementeEncadenada<Arco>>(4000);
		listaNodos=new ListaSimplementeEncadenada<T>();
	}

	@Override
	public boolean agregarNodo(T nodo) 
	{
		if(listaNodos.buscarObjetoListaOrdenada(true, nodo)!=null)
		{
			return false;
		}
		listaNodos.addLast(nodo);
		listaNodos.ordenarLista(true);
		return true;
	}

	@Override
	public boolean eliminarNodo(int id) {
		Node<T> actual= listaNodos.darPrimerNodo();
		Node<T> anterior=null;
		Paradero AEliminar=new Paradero(id, 0.0, 0.0, "");
		if(actual.compareTo((T) AEliminar)==0)
		{
			listaNodos.removeFirst();
			return true;
		}
		while(actual!=null)
		{
			if(actual.compareTo((T) AEliminar)==0)
			{
				anterior.setNext(actual.getNext());
				return true;
			}
			anterior=actual;
			actual=actual.getNext();
		}
		return false;
	}

	@Override
	public Arco[] darArcos() {
		ListaSimplementeEncadenada<Arco> arregloArcos = new ListaSimplementeEncadenada<Arco>();
		for(int i = 0; i < listaNodos.size();i++)
		{
			T actual= listaNodos.get(i);
			arregloArcos.addAll(adj.get(actual.darId()));
		}
		return (Arco[]) arregloArcos.toArray();
	}

	private <E extends Comparable<E>> Arco crearArco(  int inicio,  int fin,  double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		//TODO implementar
		return listaNodos.toArray();
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		Paradero inicio = (Paradero) listaNodos.buscarObjetoListaOrdenada(true, (T) new Paradero(i, 0.0, 0.0, ""));
		Paradero fin = (Paradero) listaNodos.buscarObjetoListaOrdenada(true, (T) new Paradero(f, 0.0, 0.0, ""));
		if(inicio == null || fin ==null)
		{
			return false;
		}
		Arco nuevo= new Arco(inicio, fin, costo,obj);
		ListaSimplementeEncadenada<Arco> lista = adj.get(i);
		if(lista==null)
		{
			lista= new ListaSimplementeEncadenada<Arco>();
			lista.addFirst(nuevo);
			adj.insert(i, lista);
			return true;
		}
		for(int n =0;n < lista.size();n++)
		{
			if(lista.get(n).compareTo(nuevo)==0)return false;
		}
		lista.addLast(nuevo);
		return true;
		
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {
		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int i, int f) {
		ListaSimplementeEncadenada<Arco> lista= adj.get(i);
		Paradero inicio= (Paradero) listaNodos.buscarObjetoListaOrdenada(true,(T) new Paradero(i, 0.0, 0.0,""));
		Paradero fin= (Paradero) listaNodos.buscarObjetoListaOrdenada(true,(T) new Paradero(f, 0.0, 0.0,""));
		if(lista==null || lista.isEmpty()) return null;
		for(int n =0;n < lista.size();n++)
		{
			Arco actual= lista.get(n);
			if(actual.darNodoInicio().darId()==i && actual.darNodoFin().darId()==f)
			{
				return lista.remove(actual);
			}
		}
		return null;
		
	}

	@Override
	public Nodo buscarNodo(int id) {
		return listaNodos.buscarObjetoListaOrdenada(true,(T) new Paradero(id, 0.0, 0.0,""));
	}

	@Override
	public Arco[] darArcosOrigen(int id) {
		return adj.get(id).toArray();
	}

	@Override
	public Arco[] darArcosDestino(int id) {
		ListaSimplementeEncadenada<Arco> buscado= new ListaSimplementeEncadenada<Arco>();
		for(int i =0; i<listaNodos.size();i++)
		{
			int idAc= listaNodos.get(i).darId();
			ListaSimplementeEncadenada<Arco> lista= adj.get(idAc);
			for(int j =0; j<lista.size();j++)
			{
				Arco actual=lista.get(j);
				if(actual.darNodoFin().darId()==id)
				{
					buscado.addLast(actual);
				}
			}
		}
		return buscado.toArray();
	}

}
