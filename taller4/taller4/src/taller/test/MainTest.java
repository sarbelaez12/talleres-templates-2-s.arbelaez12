package taller.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.HybridSortTeam;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.MergeSortTeam;
import taller.mundo.teams.QuickSortTeam;
import taller.mundo.teams.SelectionSortTeam;

public class MainTest extends TestCase{
   
  public void testSample() {
	Comparable[] arr= {2,5,3,1,4};
	//Test de arreglo burbuja
	BubbleSortTeam tBurbuja= new BubbleSortTeam();
	InsertionSortTeam tInsert = new InsertionSortTeam();
	SelectionSortTeam tSeleccion= new SelectionSortTeam();
	//Prueba bubuja
	arr=tBurbuja.sort(arr, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de burb asc",1, arr[0]);
    arr=tBurbuja.sort(arr, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de burb desc",5, arr[0]);
    //prueba insersion
    arr=tInsert.sort(arr, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de insert asc",1, arr[0]);
    arr=tInsert.sort(arr, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de insert desc",5, arr[0]);
  //prueba seleccion
    arr=tSeleccion.sort(arr, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de selec asc",1, arr[0]);
    arr=tSeleccion.sort(arr, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de selec desc",5, arr[0]);
  //prueba Merge
    MergeSortTeam tMerge= new MergeSortTeam();
    arr=tMerge.sort(arr, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de merge asc",1, arr[0]);
    arr=tMerge.sort(arr, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de mergedesc",5, arr[0]);
    
    QuickSortTeam tQuick= new QuickSortTeam();
    Comparable[] arr1= {2,5,3,1,4};
    arr1=tQuick.sort(arr1, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de quick asc",1, arr1[0]);
    arr1=tQuick.sort(arr1, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de quick desc",5, arr1[0]);
    
    HybridSortTeam tHybrid= new HybridSortTeam();
    arr=tHybrid.sort(arr, TipoOrdenamiento.ASCENDENTE);
    assertEquals("No sirvel el algoritmo de quick asc",1, arr[0]);
    arr=tHybrid.sort(arr, TipoOrdenamiento.DESCENDENTE);
    assertEquals("No sirvel el algoritmo de quick desc",5, arr[0]);
    
  }
  
}