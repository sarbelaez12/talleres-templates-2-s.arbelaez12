package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Random;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

     private static Random random = new Random();

     public QuickSortTeam()
     {
          super("Quicksort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          quicksort(list, 0, list.length, orden);
          return list;
     }
     
     private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
     {
    	 if(fin<=inicio)
    		 return;
    	 int part= particion(lista, inicio, fin, orden);
    	 quicksort(lista, 0, part-1, orden);
    	 
    	 quicksort(lista,part+1,fin,orden);
     }

    private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
    {
    	int cursorIzq=inicio;
    	int cursorDer=fin-1;
    	int pivote=eleccionPivote(inicio, fin);
    	if(orden==TipoOrdenamiento.ASCENDENTE)
    	{
    		while(cursorIzq<=cursorDer )
    		{
    			while(lista[cursorIzq].compareTo(lista[pivote])<0)
        		{
        			cursorIzq++;
        		}
    			while(lista[cursorDer].compareTo(lista[pivote])>0)
    			{
    				cursorDer--;
    			}
    			if(cursorIzq<=cursorDer)
    			{
    				Comparable temp = lista[cursorDer];
    				lista[cursorDer]=lista[cursorIzq];
    				lista[cursorIzq]=temp;
    			}
    			if(cursorIzq>=cursorDer)
    				return pivote;
    		}
    		
    		
    		
    	}else
    	{
    		while(cursorIzq<=cursorDer )
    		{
    			while(lista[cursorIzq].compareTo(lista[pivote])>0)
        		{
        			cursorIzq++;
        		}
    			while(lista[cursorDer].compareTo(lista[pivote])<0)
    			{
    				cursorDer--;
    			}
    			if(cursorIzq<=cursorDer)
    			{
    				Comparable temp = lista[cursorDer];
    				lista[cursorDer]=lista[cursorIzq];
    				lista[cursorIzq]=temp;
    			}
    			if(cursorIzq>=cursorDer)
    				return pivote;
    		}
    		
    	}
		return 0;
    }

    private static int eleccionPivote(int inicio, int fin)
    {
         return (inicio+fin)/2;
    }

    /**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
    **/
    public static int randInt(int min, int max) 
    {
          int randomNum = random.nextInt((max - min) + 1) + min;
          return randomNum;
    }
    
}
