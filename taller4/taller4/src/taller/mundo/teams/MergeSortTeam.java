package taller.mundo.teams;

import java.util.ArrayList;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 if(lista.length==1)
    		 return lista;
    	 int m = lista.length/2;
    	 Comparable[] lista1 = new Comparable[m];
    	 Comparable[] lista2 = new Comparable[lista.length-m];
    	 for(int i =0; i<lista.length;i++)
    	 {
    		 if(i<m)
    			 lista1[i]=lista[i];
    		 else
    			 lista2[i-m]=lista[i];
    	 }
    		 lista1=merge_sort(lista1, orden);
    		 lista2=merge_sort(lista2, orden);
    		 Comparable[] resultado= merge(lista1,lista2,orden);
    		 return resultado;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	Comparable[] resultado=new Comparable[izquierda.length+derecha.length];
    	int i1=0;
    	int i2=0;
    	int i3=0;
    	while (i1<izquierda.length && i2<derecha.length)
    	{
    		if(orden==TipoOrdenamiento.ASCENDENTE){
    			if(izquierda[i1].compareTo(derecha[i2])<0)
    			{
    				resultado[i3]=izquierda[i1];
        			i1++;
        			i3++;
    			}
    			else
    			{
    				resultado[i3]=derecha[i2];
        			i2++;
        			i3++;
    			}
    		}else{
    			if(izquierda[i1].compareTo(derecha[i2])>0)
    			{
    					resultado[i3]=izquierda[i1];
        				i1++;
        				i3++;
    				}
    				else
    				{
    					resultado[i3]=derecha[i2];
        				i2++;
        				i3++;
    				}
    			}
    		if(i1==izquierda.length)
    		{
    			for(int n=0;i2<derecha.length;i2++)
    			{
    				resultado[i3]=derecha[i2];
    				i3++;
    			}
    		}else if(i2==derecha.length)
    		{
    			for(int n=0;i1<izquierda.length;i1++)
    			{
    				resultado[i3]=izquierda[i1];
    				i3++;
    			}
    		}
    	}
		return resultado;
     }


}
