package taller.mundo.teams;
import javax.swing.text.AbstractDocument.LeafElement;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
public class HybridSortTeam extends AlgorithmTeam
{

	public HybridSortTeam() {
		super("Hybrid sort");
		userDefined=true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden) 
	{
		return HybridSort(list,orden);
	}

	private Comparable[] HybridSort(Comparable[] list, TipoOrdenamiento orden) 
	{
		if(list.length==0 || list.length==1)
			return list;
		if(list.length==2)
		{
			
			if(orden==TipoOrdenamiento.ASCENDENTE)
			{
				if(list[0].compareTo(list[1])>0)
				{
					Comparable temp= list[0];
				}
				return list;
			}else
			{
				if(list[0].compareTo(list[1])<0)
				{
					Comparable temp= list[0];
					list[0]=list[1];
					list[1]=temp;
				}
				return list;
			}
		}else
		{
			int m= list.length/3;
			Comparable[] lista1= new Comparable[m];
			Comparable[] lista2= new Comparable[m];
			Comparable[] lista3= new Comparable[list.length-m*2];
			for(int i=0;i<list.length;i++)
			{
				if(i<m)
				{
					lista1[i]=list[i];
				}
				else if(i<m*2)
				{
					lista2[i-m]=list[i];
				}else
				{
					
					lista3[i-m*2]=list[i];
					
				}
			}
			lista1=HybridSort(lista1, orden);
			lista2=HybridSort(lista2, orden);
			lista3=HybridSort(lista3, orden);
			return merge(merge(lista1,lista2,orden),lista3,orden);
			
		}
	}

	private Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden) {
	Comparable[] resultado=new Comparable[izquierda.length+derecha.length];
	int i1=0;
	int i2=0;
	int i3=0;
	while (i1<izquierda.length && i2<derecha.length)
	{
		if(orden==TipoOrdenamiento.ASCENDENTE){
			if(izquierda[i1].compareTo(derecha[i2])<0)
			{
				resultado[i3]=izquierda[i1];
    			i1++;
    			i3++;
			}
			else
			{
				resultado[i3]=derecha[i2];
    			i2++;
    			i3++;
			}
		}else{
			if(izquierda[i1].compareTo(derecha[i2])>0)
			{
					resultado[i3]=izquierda[i1];
    				i1++;
    				i3++;
				}
				else
				{
					resultado[i3]=derecha[i2];
    				i2++;
    				i3++;
				}
			}
		if(i1==izquierda.length)
		{
			for(int n=0;i2<derecha.length;i2++)
			{
				resultado[i3]=derecha[i2];
				i3++;
			}
		}else if(i2==derecha.length)
		{
			for(int n=0;i1<izquierda.length;i1++)
			{
				resultado[i3]=izquierda[i1];
				i3++;
			}
		}
	}
	return resultado;
 }
		
	
}