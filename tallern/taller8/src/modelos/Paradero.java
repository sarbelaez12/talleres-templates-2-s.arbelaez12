package modelos;

import estructuras.Nodo;

/**
 * Clase que representa un paradero del sistema integrado de transporte SITP
 * @author SamuelSalazar
 */
//TODO La clase debe implementar la interface Nodo
public class Paradero implements Nodo{

	/**
	 * Latitud del paradero
	 */
	private double lat;
	//TODO declare el atributo latitud
	
	/**
	 * Longitud del paradero
	 *
	 */
	private double lon;
	//TODO Declare el atributo longitud
	
	/**
	 * Nombre del paradero
	 */
	private String nombre;
	//TODO declare el nombre del paradero
	
	/**
	 * Identificador unico del paradero
	 */
	private int id;
	//TODO Declare el identificador del paradero
	
	
	/**
	 * Construye un nuevo paradero con un nombre e id dado en una ubicación
	 * representada por la latitud y la longitud.
	 * @param id Indentificador unico del paradero
	 * @param latitud 
	 * @param longitud 
	 * @param nombre
	 */
	public Paradero(int id, double latitud, double longitud, String nombre) {
		//TODO implementar
		lat=latitud;
		lon = longitud;
		this.id=id;
		this.nombre=nombre;
	}
	
	public int darId(){
		//TODO implementar
		return id;
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		//TODO implementar
		return lat;
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		//TODO implementar
		return lon;
	}
	
	/**
	 * Devuelve el nombre del paradero
	 * @return nombre
	 */
	public String darNombre() {
		//TODO implementar
		return nombre;
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return nombre+" ("+lat+" , "+lon+")";
	}

	@Override
	public int compareTo(Nodo o) 
	{
		return o.darId() - id;
	}
	
	
}
