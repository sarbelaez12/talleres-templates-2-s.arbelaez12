package estructuras;

public class ListaSimplementeEncadenada<E>
{
	//Clase nodo
	public static class Node<E>
	{
		private E element;
		private Node<E> next;
		public Node(E e, Node<E> n)
		{
			element=e;
			next=n;
		}
		public E getElement() { return element;}
		public Node<E> getNext() { return next;}
		public void setNext(Node<E> n){ next=n;}
		public int compareTo(E o) {
			return ((Comparable<E>) element).compareTo(o);
		}
	}
	private Node<E> head = null;
	private Node<E> tail = null;
	private int size=0;
	private Node<E> actual=null;
	//Constructor de la lista 
	public ListaSimplementeEncadenada(){}
	public int size() { return size;}
	public boolean isEmpty(){ return (size==0);}
	public E first()
	{
		return  ((isEmpty())?null:head.getElement());
	}
	public E last()
	{
		return  ((isEmpty())?null:tail.getElement());
	}
	public void addFirst(E e)
	{
		head= new Node<E>(e,head);
		if(size==0)
		{
			tail=head;
		}
		size++;
	}
	public Node<E> darPrimerNodo()
	{
		actual=head;
		return actual;
	}
	public void addLast(E e)
	{
		Node<E> newest= new Node<E>(e,null);
		if(isEmpty())
		{
			head=newest;
			
		}else
		{
			tail.setNext(newest);
		}
		tail=newest;
		size++;
	}
	public E removeFirst()
	{
		if(isEmpty()) return null;
		E answer = head.getElement();
		head=head.getNext();
		size--;
		if(size==0) tail=null;
		return answer;
	}
	public E remove(int i)
	{
		if(i>=size || i<0) throw new IndexOutOfBoundsException();
		else if(i == 0)
		{
			E removido = head.getElement();
			removeFirst();
			return removido;
		}
		else
		{
			int contador = 0;

			Node<E> presentNode = head;
			Node<E> nextNode = head.getNext();

			while(contador<(i-1))
			{
				presentNode = presentNode.getNext();
				nextNode = presentNode.getNext();
				contador++;
			}
			presentNode.setNext(nextNode.getNext());
			
			if(i==size-1) tail = presentNode;
			
			size--;
			return nextNode.getElement();
		}
	}
	public E get(int i)
	{
		if(i>=size || i<0) throw new IndexOutOfBoundsException();
		else if(i==size-1) return tail.getElement();
		else
		{
			int contador = 0;

			Node<E> nextNode = head;

			while(contador<i)
			{
				nextNode =nextNode.getNext();
				contador++;
			}
			return nextNode.getElement();
		}
	}
	public void replace(E e, int i)
	{
		if(i>=size || i<0) throw new IndexOutOfBoundsException();
		else if(i==0)
		{
			head=new Node<E>(e,head.getNext());
			if(i==size-1) tail = head;
		}
		else
		{
			Node<E> actual= head;
			Node<E> anterior=null;
			for(int n=0; n<i;n++)
			{
				anterior=actual;
				actual=actual.getNext();
			}
			Node<E> nuevo = new Node<E>(e, actual.getNext());
			anterior.setNext(nuevo);
			if(i==size-1) tail = nuevo;
			actual=null;
		}
	}
	public void ordenarLista(Boolean esAscendente)
	{
		if(head==null)
			return;
		if(head.next==null)
			return;
		head=merge_sort(head, esAscendente);
		Node<E> actual=head.next;
		while(actual.next!=null)
			actual=actual.next;
		tail=actual;
	}
	private Node<E> merge_sort(Node<E> actual,Boolean esAscendente)
	{
		if(actual.next==null)
		{
			return actual;
		}
		else
		{
			Node<E> respuesta=null;
			Node<E> rapido=actual.next;
			Node<E> lento=actual;
			Boolean hay=true;
			Node<E> lista2=null;
			Node<E> lista1=actual;
			while(hay)
			{
				if(rapido.next==null)
				{
					lista2=lento.next;
					lento.setNext(null);
					hay=false;
				}
				else if(rapido.next.next==null)
				{
					lista2=lento.next;
					lento.setNext(null);
					hay=false;
				}
				else
				{
					rapido=rapido.next.next;
					lento=lento.next;
				}
			}
			lista1=merge_sort(lista1,esAscendente);
			lista2=merge_sort(lista2, esAscendente);
			respuesta= merge(lista1,lista2,esAscendente);
			return respuesta;
		}
	}
	private Node<E> merge(Node<E> izq, Node<E> der, Boolean esAscendente)
	{
		Node<E> resultado=null;
		Node<E> actual=null;
		while(izq!=null && der!=null)
		{
			if(esAscendente)
			{
				if(izq.compareTo(der.getElement())<0)
				{
					Node<E> nuevo= new Node<E>(izq.getElement(), null);
					if(resultado==null)
					{
						resultado=nuevo;
						actual=resultado;
						
					}
					else
					{
						actual.setNext(nuevo);
						actual=actual.next;
						
					}
					izq=izq.next;
				}else
				{
					Node<E> nuevo= new Node<E>(der.getElement(), null);
					if(resultado==null)
					{
						resultado=nuevo;
						actual=resultado;
					}
					else
					{
						actual.setNext(nuevo);
						actual=actual.next;
						
					}
					der=der.next;
				}
			}else
			{
				if(izq.compareTo(der.getElement())>0)
				{
					Node<E> nuevo= new Node<E>(izq.getElement(), null);
					if(resultado==null)
					{
						resultado=nuevo;
						actual=resultado;
					}
					else
					{
						actual.setNext(nuevo);
						actual=actual.next;
						
					}
					izq=izq.next;
				}else
				{
					Node<E> nuevo= new Node<E>(der.getElement(), null);
					if(resultado==null)
					{
						resultado=nuevo;
						actual=resultado;
					}
					else
					{
						actual.setNext(nuevo);
						actual=actual.next;
						
					}
					der=der.next;
				}
			}
		}
		//Termina el while
		if(izq==null)
		{
			if(resultado==null)
			{
				actual=resultado=der;
			}else
			{
				actual.setNext(der);
			}
		}
		else if(der==null)
		{
			if(resultado==null)
			{
				actual=resultado=izq;
			}else
			{
				actual.setNext(izq);
			}
		}
		return resultado;
	}
	public E buscarObjetoListaOrdenada(Boolean esAscendente, E ObjetoCodigo)
	{
		Node<E> actual= head;
		Boolean seguir=true;
		while(seguir)
		{
			if(actual==null)
			{
				seguir=false;
				break;
			}
			else if(actual.compareTo(ObjetoCodigo)==0)
			{
				return actual.getElement();
			}
			else if(!esAscendente)
			{
				if(actual.compareTo(ObjetoCodigo)<0)
				{
					seguir=false;
					break;
				}else
					actual=actual.next;
			}
			else
			{
				if(actual.compareTo(ObjetoCodigo)>0)
				{
					seguir=false;
					break;
				}else
					actual=actual.next;
			}
		}
		return null;
	}
	public E[] toArray()
	{
		Object[] lista= new Object[size];
		Node actual= head;
		int contador=0;
		while(actual!=null)
		{
			lista[contador]=actual.getElement();
			actual=actual.getNext();
			contador++;
		}
		return (E[])lista;
	}
	public void addAll(ListaSimplementeEncadenada<E> listaSimplementeEncadenada) {
		Node actual= listaSimplementeEncadenada.darPrimerNodo();
		while(actual!=null)
		{
			addLast((E) actual.getElement());
			actual=actual.getNext();
		}
		
	}
	public E remove(E obj)
	{
		if(isEmpty()) return null;
		Node actual=head;
		Node anterior=null;
		if(actual.compareTo(obj)==0)
		{
			return removeFirst();
		}
		while(actual!=null)
		{
			if(actual.compareTo(obj)==0)
			{
				anterior=actual.getNext();
				return (E) actual.getElement();
			}
		}
		return null;
	}


	
}
