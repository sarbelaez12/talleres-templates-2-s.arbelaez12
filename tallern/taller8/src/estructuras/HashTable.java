package estructuras;

public class HashTable<K,V>{
	private Hashnode<K,V>[] nodes;
	private int tamTabla;
	private int numValores;
	
	static class Hashnode<K,V> {
		final K key;
		V data;
		Hashnode<K,V> next;
		final int hash;

		public Hashnode(K k, V v, Hashnode<K,V> n, int h){
			key = k;
			data = v;
			next = n;
			hash = h;
		}
	}

	@SuppressWarnings("unchecked")
	public HashTable(int size){
		int sigPrimo = encontrarSiguientePrimo(size);
		nodes = new Hashnode[sigPrimo];
		tamTabla = sigPrimo;
		numValores = 0;
	}

	public int encontrarSiguientePrimo(int size)
	{
		for(int i = size; true;i++)
		{
			boolean esPrimo = true;
			for(int j=2;j<i && esPrimo;j++)
			{
				if(i%j==0)
				{
					esPrimo = false;
				}
			}
			if(esPrimo) return i;
		}
	}
	
	private int getIndex(K key){
		int hash = key.hashCode() % nodes.length;
		if(hash < 0){
			hash += nodes.length;
		}
		return hash;
	}

	public V insert(K key, V data){
		int hash = getIndex(key);

		for(Hashnode<K,V> node = nodes[hash]; node != null; node = node.next){
			if((hash == node.hash) && key.equals(node.key)){
				V oldData = node.data;
				node.data = data;
				return oldData;
			}
		}

		Hashnode<K,V> node = new Hashnode<K,V>(key, data, nodes[hash], hash);
		nodes[hash] = node;
		numValores++;
		return null;
	}

	public boolean remove(K key){
		int hash = getIndex(key);
		Hashnode<K,V> previous = null;
		for(Hashnode<K,V> node = nodes[hash]; node != null; node = node.next){
			if((hash == node.hash) && key.equals(node.key)){
				if(previous != null){
					previous.next = node.next;
				}else{
					nodes[hash] = node.next;
				}
				numValores--;
				return true;
			}
			previous = node;    
		}
		return false;
	}

	public V get(K key){
		int hash = getIndex(key);

		for(Hashnode<K,V> node = nodes[hash]; node != null; node = node.next){
			if(key.equals(node.key))
				return node.data;
		}
		return null;
	}

	public void resize(int size){
		int sigPrimo = encontrarSiguientePrimo(size);
		HashTable<K, V> newtbl = new HashTable<K, V>(sigPrimo);
		for(Hashnode<K,V> node : nodes){
			for(; node != null; node = node.next){
				newtbl.insert(node.key, node.data);
				remove(node.key);
			}
		}
		nodes = newtbl.nodes;
		tamTabla = sigPrimo;
	}

	public int size()
	{
		return numValores;
	}
	
	public int tamTabla()
	{
		return tamTabla;
	}
	
	public static void main(String[] args)
	{
		int i=23;
		System.out.println("El n�mero primo mayor y m�s cercano a "+i+" es "+new HashTable<Integer, Integer>(i).encontrarSiguientePrimo(i));
	}
	
}
