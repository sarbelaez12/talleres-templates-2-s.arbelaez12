package taller.mundo;

public class Producto 
{
	private String nombre;
	private long tiempoPreparacion;
	private long costo;
	public Producto (String pNombre, long pTiempoPreparacion, long pCosto)
	{
		nombre=pNombre;
		tiempoPreparacion=pTiempoPreparacion;
		costo=pCosto;
	}
	public String darNombre()
	{
		return nombre;
		
	}
	public long darTiempoPreparacion()
	{
		return tiempoPreparacion;
	}
	public long darCosto()
	{
		return costo;
	}

}
