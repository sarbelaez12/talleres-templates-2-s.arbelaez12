package taller.mundo;

import java.util.Iterator;

import taller.estructuras.Cola;
import taller.estructuras.ListaDobleEncadenada;

public class AdministracionMcDonalds{
	
	private Cola<String> colaClientes;
	private Cola<Pedido> colaPedidos;
	private Cola<Pedido> colaDomicilios;
	private ListaDobleEncadenada<Producto> catalogoProductos;
	
	
	public AdministracionMcDonalds(){
		colaClientes=new Cola<String>();
		colaPedidos=new Cola<Pedido>();
		colaDomicilios=new Cola<Pedido>();
		catalogoProductos= new ListaDobleEncadenada<Producto>();
		iniciarProductosIniciales();

	}
	
	private void iniciarProductosIniciales() {
		Producto n1= new Producto("Combo Nuggets", 200, 8000);
		Producto n2= new Producto("Combo Bigmac", 300, 12000);
		Producto n3= new Producto("Combo Cuarto de Libra", 250, 14000);
		Producto n4= new Producto("McFlurry", 100, 5000);
		catalogoProductos.AddLast(n1);
		catalogoProductos.AddLast(n2);
		catalogoProductos.AddLast(n3);
		catalogoProductos.AddLast(n4);

		
	}

	public void agregarCliente(String nombre){
		colaClientes.encolar(nombre);
	}
	
	public String atenderCliente(Pedido pedido){
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {

		}
		if(pedido.esDomicilio())
		{
			colaDomicilios.encolar(pedido);
		}else
		{
			colaPedidos.encolar(pedido);
		}
		
		return colaClientes.desencolar();
	}
	
	public String entregarPedido(){
		Pedido p = colaPedidos.desencolar();
		try {
			Thread.sleep(p.getTiempoProceso());
		} catch (InterruptedException e) {

		}
		return p.getNombre();
	}
	public String entregarDomicilio(){
		Pedido p = colaDomicilios.desencolar();
		try {
			Thread.sleep(p.getTiempoProceso());
		} catch (InterruptedException e) {

		}
		return p.getNombre();
	}
	
	public int clientesEnFila(){
		return colaClientes.tamanio();
	}
	
	public int pedidosEnEspera(){
		return colaPedidos.tamanio();
	}
	public int domiciliosEnEspera(){
		return colaDomicilios.tamanio();
	}
	
	public Iterator<String> clientes(){
		return colaClientes.iterator();
	}
	
	public Iterator<Pedido> pedidos(){
		return colaPedidos.iterator();
	}
	public Iterator<Pedido> domicilios(){
		return colaDomicilios.iterator();
	}
	public ListaDobleEncadenada<Producto> getCatalogoProductos()
	{
		return catalogoProductos;
	}

	public void agregarDomicilio(Pedido or) {
		
		colaDomicilios.encolar(or);
	}

	public void agregarProducto(Producto nuevo) {
		catalogoProductos.AddLast(nuevo);
		
	}

	public void eliminarProducto(int lugar) {
		catalogoProductos.remove(lugar);
		
	}

}
