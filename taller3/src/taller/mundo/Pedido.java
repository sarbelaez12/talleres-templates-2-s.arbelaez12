package taller.mundo;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Pedido 
{
	private Producto producto;
	private Calendar hora;
	private Boolean domicilio;
	
	public Pedido(Producto pProducto, boolean esDomicilio)
	{
		producto = pProducto;
		hora=Calendar.getInstance();
		domicilio=esDomicilio;
	}
	public Producto darProducto()
	{
		return producto;
	}
	public Calendar darHoraPedido()
	{
		return hora;
	}
	public Boolean esDomicilio()
	{
		return domicilio;
	}
	public long getTiempoProceso() {
		return producto.darTiempoPreparacion();
	}
	public String getNombre() {
		return producto.darNombre();
	}

}
