/*
 * CentralPacienteCLI.java
 * This file is part of ISIS1206
 *
 * Copyright (C) 2015 - ISIS1206 Team
 */
package taller.interfaz;

import java.util.Iterator;
import java.util.Scanner;

import taller.estructuras.NodoLista;
import taller.mundo.*;


public class McDonaldsCLI
{

	private AdministracionMcDonalds admin;

	private Scanner in;

	public McDonaldsCLI() {
		admin = new AdministracionMcDonalds();
		in = new Scanner(System.in);
	}

	public void mainMenu() throws InterruptedException {
		boolean finish = false;
		while (!finish) {
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-        McDonalds - Candelaria          -");
			System.out.println("                                                        \n"+
					"                  ╥╣▒▒▒▒@,        ╥╢▒▒▒▒@╖                            \n"+
					"                ╥▒▒▒▒▄▒▒▒▒▀█▄   ╓╣▒▒▒▄▒▒▒▒▒█▄                         \n"+
					"               ╣▒▒▒▒███╢▒▒▒▒██ ╓▒▒╣▒███╚▒▒╢▒██                        \n"+
					"              ╣▒▒▒▒███  ▒▒▒▒▒█▌▒▒╣▒▓██  ║▒▒▒▒██▄                      \n"+
					"             ╣▒▒▒▒███   ]▒▒╢▒▒╣░║▒▒██U   ╢▒▒▒▒██µ                     \n"+
					"            ║▒▒╣▒▐██     ▒▒▒▒▒░░▒▒▐██     ▒▒▒▒▒██                     \n"+
					"            ╣▒▒▒▒███     ╢▒▒╢╣░║▒▒██▌     ╢▒▒▒▒▀██                    \n"+
					"           ╢▒▒╢▒▒██      ]▒▒▒╣░▒▒▒██      ]▒▒▒▒▒██µ                   \n"+
					"           ╣▒▒▒▒███       ▒▒▒╣░▒▒▐██       ▒▒▒▒▒▀██                   \n"+
					"          ║░░╢▒▒██▌       ▒▒░╢║▒▒██▌       ║▒▒╢▒▒██                   \n"+
					"          ▒░▒▒▒▒██        ╢▒░▒╢▒▒██▌       ]▒▒▒▒▒██▌                  \n"+
					"          ╣░▒▒▒▐██        ║▒░▒▒▒▒██U        ▒▒▒▒▒▐██                  \n"+
					"         ]▒░▒▒▒▐██        ║▒░▒▒▒▒██         ▒▒░▒▒▒██                  \n"+
					"         ║▒░║▒▒██▌        ]▒▒╣▒▒▒██         ▒▒░▒▒▒██                  \n"+
					"         ║▒╢▒▒▒██▌         ╙▓██████         ▒▒╢▒▒▒██                \n"+
					"         ``██████▌          ╙▀▀▀▀▀▀         ``██████                ");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("Bienvenido McDonalds\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Agregar cliente a la cola");
			System.out.println("2. Atender cliente");
			System.out.println("3. Entregar pedido");
			System.out.println("4. Cantidad de clientes en cola");
			System.out.println("5. Cola de pedidos en cocina");
			System.out.println("6. Agregar Domicilio");
			System.out.println("7. Despachar Domicilio");
			System.out.println("8. Ver lista de  Domicilios");
			System.out.println("9. Ver catalogo de productos");
			System.out.println("10. Agregar Producto al catalogo");
			System.out.println("11. Eliminar producto del catalogo");
			System.out.println("12. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = in.nextInt();
			in.nextLine();
			switch (opt1) {
			case 1:
				agregarCliente();
				break;
			case 2:
				atenderCliente();
				break;
			case 3:
				entregarPedido();
				break;
			case 4:
				numeroClientes();
				break;
			case 5:
				pedidos();
				break;
			case 6:
				agregarDomicilio();
				break;
			case 7:
				DespacharDomicilio();
				break;
			case 8:
				verListaDomicilios();
				break;
			case 9:
				verCatalogoProductos();
				break;
			case 10:
				agregarProducto();
				break;
			case 11:
				eliminarProducto();
				break;
			case 12:
				finish = true;
				break;
			default:
				break;
			}
		}
	}


	private void eliminarProducto() {
		
		if(admin.getCatalogoProductos().size()==0)
		{
			System.out.println("no hay productos en el catalogo");
			System.out.println("----------------------------------");
			System.out.println("Presione enter para volver al menú principal");
			in.nextLine();
		}
		System.out.println("eliminar producto del catalogo");
		System.out.println("----------------------------------");
		NodoLista<Producto> actual = admin.getCatalogoProductos().first();
		int contador=1;
		while(actual!=null){
			System.out.println("Producto: "+contador+"."+actual.getElement().darNombre());
			contador++;
			actual=actual.getNext();
		}
		Boolean ya = false;
		int lugar=0;
		
		while(!ya)
		{
			System.out.println("Ingrese el numero del producto que desea borrar");
			lugar=in.nextInt();
			in.nextLine();
			if(lugar>=0 && lugar <contador-1)
			{
				ya=true;
			}
		}
		admin.eliminarProducto(lugar-1);
		System.out.println("Se elimino correctamente el producto");
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
	}

	private void agregarProducto() {
		System.out.println("Agregar Producto al catalogo");
		System.out.println("----------------------------------");
		Boolean ya=false;
		String nombre = null;
		long tiempo = 0;
		long precio = 0;
		while(!ya)
		{
			System.out.println("Ingrese el nombre del producto");
			nombre=in.nextLine();
			if(nombre!=null && nombre!="")
			{
				ya=true;
			}
			
		}
		ya=false;
		while(!ya)
		{
			System.out.println("Ingrese el tiempo de preparacion (Mayor a 0)");
			tiempo=Integer.parseInt(in.nextLine());
			if(tiempo>0)
			{
				ya=true;
			}
			
		}
		ya=false;
		while(!ya)
		{
			System.out.println("Ingrese el Precio del producto (Mayor a 0)");
			precio=(long) Double.parseDouble(in.nextLine());
			if(precio>0)
			{
				ya=true;
			}
			
		}
		Producto nuevo= new Producto(nombre, tiempo, precio);
		admin.agregarProducto(nuevo);
		System.out.println("Se agrego correctamente el producto al catálogo");
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		
	}

	private void verCatalogoProductos() {
		System.out.println("Listar Catalogo de productos");
		System.out.println("----------------------------------");
		NodoLista<Producto> actual = admin.getCatalogoProductos().first();
		while(actual!=null){
			System.out.println("Producto: "+actual.getElement().darNombre());
			actual=actual.getNext();
		}
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		
	}

	private void verListaDomicilios() {
		System.out.println("Listar Domicilios");
		System.out.println("----------------------------------");
		Iterator<Pedido> it = admin.domicilios();
		while(it.hasNext()){
			System.out.println("Domicilio: "+it.next().getNombre());
		}
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		
	}

	private void DespacharDomicilio() {
		if(admin.domiciliosEnEspera()==0)
		{
			System.out.println("No hay domicilios en la lista");
			System.out.println("Presione enter para volver al menú principal");
			in.nextLine();
			return;
		}
		System.out.println("Entregar el primer domicilio en lista ");
		System.out.println("----------------------------------");
		System.out.println("Prepadando domicilio porfavor espere .......");
		String pedido = admin.entregarDomicilio();
		System.out.println("Se entregó el domicilio: "+pedido);
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		
	}

	private void agregarDomicilio() {
		System.out.println("Agregar un Domicilio:");
		System.out.println("----------------------------------");
		System.out.println("Escoja la orden del cliente:");
		NodoLista<Producto>  actual= admin.getCatalogoProductos().first();
		for (int i=1 ; i<= admin.getCatalogoProductos().size();i++)
		{
			System.out.println(i+". "+actual.getElement().darNombre());
			actual=actual.getNext();
		}
		int orden = in.nextInt();
		Pedido or = null;
		if(orden>=1 && orden <= admin.getCatalogoProductos().size())
		{
			or= new Pedido(admin.getCatalogoProductos().dar(orden-1), true);
		}
		in.nextLine();
		admin.agregarDomicilio(or);
		System.out.println("Se agregó correctamente el domicilio");
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		
		
	}

	private void agregarCliente() throws InterruptedException {
		boolean finish = false;
		String nombre = "";
		while (!finish) {
			System.out.println("Agregar un cliente nuevo a la cola del restaurante");
			System.out.println("----------------------------------");
			System.out.println("Ingrese el nombre del cliente:");
			nombre = in.nextLine();
			admin.agregarCliente(nombre);
			System.out.println("Se agregó el cliente llamando " + nombre + " a la cola del restaurante");
			System.out.println("Presione enter para volver al menú principal");
			in.nextLine();
			finish = true;
		}
		


	}

	private void atenderCliente() throws InterruptedException {
		boolean finish = false;
		if(admin.clientesEnFila() ==0)
		{
			System.out.println("No hay clientes en la fila");
			return;
		}
		while (!finish) {
			System.out.println("Atender al primer cliente de la cola");
			System.out.println("Es domicilio? \n 1.Si \n 2. No");
			int dom =in.nextInt();
			Boolean esDomicilio;
			if(dom==1)
			{
				esDomicilio=true;
			}else{
				esDomicilio=false;
			}
			in.nextLine();
			System.out.println("----------------------------------");
			System.out.println("Escoja la orden del cliente:");
			NodoLista<Producto>  actual= admin.getCatalogoProductos().first();
			for (int i=1 ; i<= admin.getCatalogoProductos().size();i++)
			{
				System.out.println((i)+". "+actual.getElement().darNombre());
				actual=actual.getNext();
			}
			int orden = in.nextInt();
			Pedido or = null;
			in.nextLine();
			if(orden>=1 && orden <= admin.getCatalogoProductos().size())
			{
				or= new Pedido(admin.getCatalogoProductos().dar(orden), esDomicilio);
			}
			System.out.println("atendiendo cliente por favor espere...");
			String nombre = admin.atenderCliente(or);
			System.out.println("Se agregó la orden " + orden + " a la cola de pedidos de la cocina, a nombre del cliente "+nombre);
			System.out.println("Presione enter para volver al menú principal");
			in.nextLine();
			finish = true;
		}
		

	}

	private void entregarPedido() throws InterruptedException {
		if(admin.pedidosEnEspera()==0)
		{
			System.out.println("No hay pedidos en la cocina");
			return;
		}
		System.out.println("Entregar el primer pedido en lista de la cocina");
		System.out.println("----------------------------------");
		System.out.println("Prepadando pedido porfavor espere .......");
		String pedido = admin.entregarPedido();
		System.out.println("Se entregó el pedido: "+pedido);
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		Thread.sleep(3000);
	}

	private void numeroClientes() throws InterruptedException {
		System.out.println("Número de clientes esperando en fila");
		System.out.println("----------------------------------");
		int num = admin.clientesEnFila();
		System.out.println("Hay "+num+" clientes esperando en la fila.");
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		Thread.sleep(3000);
	}

	private void pedidos() throws InterruptedException {
		System.out.println("Listar pedidos en cocina");
		System.out.println("----------------------------------");
		Iterator<Pedido> it = admin.pedidos();
		while(it.hasNext()){
			System.out.println("Pedido: "+it.next().getNombre());
		}
		System.out.println("Presione enter para volver al menú principal");
		in.nextLine();
		Thread.sleep(3000);
	}

}