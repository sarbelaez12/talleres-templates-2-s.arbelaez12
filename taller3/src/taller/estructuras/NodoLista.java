package taller.estructuras;

public class NodoLista<T> 
{
	private NodoLista<T> siguiente;
	private NodoLista<T> anterior;
	private T valor;
	
	public NodoLista(T pValor)
	{
		valor=pValor;
		siguiente=null;
		anterior=null;
		
	}
	public T getElement()
	{
		return valor;
	}
	public void setElement(T elemento)
	{
		valor=elemento;
	}
	public NodoLista<T> getNext()
	{
		return siguiente;
	}
	public NodoLista<T> getPrev()
	{
		return anterior;
	}
	public void setNext(NodoLista<T> nuevo)
	{
		siguiente=nuevo;
	}
	public void setPrev(NodoLista<T> nuevo)
	{
		anterior=nuevo;
	}
}
