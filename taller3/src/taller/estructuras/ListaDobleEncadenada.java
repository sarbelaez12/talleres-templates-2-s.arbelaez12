package taller.estructuras;

public class ListaDobleEncadenada<T> 
{
	private NodoLista<T> cabeza;
	private NodoLista<T> cola;
	private int tamanio;
	
	public int size()
	{
		return tamanio;
	}
	public boolean isEmpty()
	{
		return tamanio==0;
	}
	public void AddFirst(T element)
	{
		NodoLista<T> nuevo = new NodoLista<T>(element);
		if(cabeza==null)
		{
			cabeza=cola=nuevo;
			tamanio++;
		}
		else
		{
			cabeza.setPrev(nuevo);
			nuevo.setNext(cabeza);
			cabeza=nuevo;
			tamanio++;
		}
	}
	public void AddLast(T element)
	{
		NodoLista<T> nuevo = new NodoLista<T>(element);
		if(cola==null)
		{
			cabeza=cola=nuevo;
			tamanio++;
		}
		else
		{
			cola.setNext(nuevo);
			nuevo.setPrev(cola);
			cola=nuevo;
			tamanio++;
		}
	}
	public T removeFirst()
	{
		if(isEmpty()) return null;
		NodoLista<T> req = cabeza;
		cabeza=cabeza.getNext();
		if(cabeza!=null)cabeza.setPrev(null);
		tamanio--;
		return req.getElement();	
	}
	public T removeLast()
	{
		if(isEmpty()) return null;
		NodoLista<T> req = cola;
		cola=cola.getPrev();
		if(cola!=null)cola.setPrev(null);
		tamanio--;
		return req.getElement();	
	}
	public NodoLista<T> first()
	{
		return cabeza;
	}
	public NodoLista<T> last()
	{
		return cola; 
	}
	public T dar(int i)
	{
		if(i>=tamanio) throw new IndexOutOfBoundsException();
		NodoLista<T> actual= cabeza;
		for(int n = 0; n<i;n++)
		{
			actual=actual.getNext();
		}
		return actual.getElement();
	}
	public void remove (int i)
	{
		if(i<0 || i>=tamanio) throw new IndexOutOfBoundsException();
		NodoLista<T> actual= cabeza;
		for(int c =0; c<i;c++)
		{
			actual=actual.getNext();
		}
		NodoLista<T> prev = actual.getPrev();
		NodoLista<T> next = actual.getNext();
		if(actual==cabeza)
		{
			cabeza=cabeza.getNext();
		}
		if(actual==cola)
		{
			cola=cola.getPrev();
		}
		if(prev!=null)
		{
			prev.setNext(next);
		}
		if(next!=null)
		{
			next.setPrev(prev);
		}
		
		
		
		
	}
	
}
