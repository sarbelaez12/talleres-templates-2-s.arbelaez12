package Mundo;

import taller.estructuras.TablaHash;
import taller.estructuras.TablaHashMultiplesValores;

public class Directorio {
	private TablaHash<Integer, Contacto> tablaTelefonos;
	private TablaHashMultiplesValores<String, Contacto> tablaApellidos;
	
	public Directorio()
	{
		tablaTelefonos= new TablaHash<>(500000,(float)0.6);
		tablaApellidos= new TablaHashMultiplesValores<>(500000,(float)0.6);
	}
	public void agregarContacto(Contacto pCon)
	{
		tablaTelefonos.put(pCon.getTelefono(), pCon);
		tablaApellidos.put(pCon.getNombre(), pCon);
	}
	public Contacto buscarContactoPorTelefono(Integer pTelefono)
	{
		return tablaTelefonos.get(pTelefono);
	}
	public Contacto[] buscarContactoPorApellidoYNombre(String nombre)
	{
		return tablaApellidos.get(nombre);
	}
}
