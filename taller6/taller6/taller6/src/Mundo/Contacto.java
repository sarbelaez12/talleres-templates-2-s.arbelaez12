package Mundo;

public class Contacto {
	private String nombre;
	private String apellido;
	private Integer telefono;
	
	public Contacto(String pNombre,String pApellido, Integer pTelefono)
	{
		nombre=pNombre;
		apellido=pApellido;
		this.telefono=pTelefono;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public String toString()
	{
		return "Nombre" + nombre + " ,Apellido:" + apellido + ", Teléfono:" + telefono;
	}

}
