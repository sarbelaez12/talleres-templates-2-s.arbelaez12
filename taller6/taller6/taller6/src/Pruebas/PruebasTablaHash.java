package Pruebas;

import junit.framework.TestCase;
import taller.estructuras.TablaHash;

public class PruebasTablaHash extends TestCase
{
	private TablaHash<Integer, String> tabla;
	private void setUpEscenario1()
	{
		tabla= new TablaHash<>();
		for(int i =1; i <=10;i++)
		tabla.put(i, ""+i);
		
	}
	public void testPut()
	{
		setUpEscenario1();
		for(int i =1; i<=10;i++)
			assertEquals("No encontro el objeto",""+i, tabla.get(i));
	}
	public void testGet()
	{
		setUpEscenario1();
		for(int i =1; i<=10;i++)
			assertEquals("No encontro el objeto",""+i, tabla.get(i));
		assertNull("No debio encontrar un objeto",tabla.get(11));
	}
	public void testDelite()
	{
		setUpEscenario1();
		tabla.delete(1);
		assertNull("No debio encontrar algo",tabla.get(1));
	}
	public void testRezise()
	{
		tabla=new TablaHash<>((int)10,(float)0.4);
		for(int i =1; i<=20;i++)
			tabla.put(i,""+i);
		for(int i =1;i<=20;i++)
		{
			assertEquals("Debería encontrar el objeto",i+"", tabla.get(i));
		}
			
	}

	
}
