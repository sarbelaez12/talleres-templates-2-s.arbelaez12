package Pruebas;

import junit.framework.TestCase;
import taller.estructuras.TablaHash;
import taller.estructuras.TablaHashMultiplesValores;

public class TablaHashMultipleTest extends TestCase{
	private TablaHashMultiplesValores<Integer, String> tabla;
	private void SetupEscenario1()
	{
		tabla= new TablaHashMultiplesValores<>(); 
		for(int i =0; i <3;i++)
		{
			for(int j =1;j<=10;j++)
				tabla.put(i, ""+j);
		}
	}
	public void testPut()
	{
		SetupEscenario1();
		assertEquals(1,1);
	}
	public void TestGet()
	{
		SetupEscenario1();
		for(int i =0; i <3;i++)
		{
			String[] arreglo= tabla.get(i);
			for(int j =1;j<=10;j++)
				assertEquals("" ,j+"" ,arreglo[j-1]);
		}
	}
}
