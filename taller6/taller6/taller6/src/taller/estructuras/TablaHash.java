package taller.estructuras;

import javax.xml.soap.Node;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash[] arreglo;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		capacidad=10;
		count=0;
		factorCargaMax=(float) 0.5;
		factorCarga=0;
		arreglo= new NodoHash[capacidad];
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		count=0;
		factorCarga=0;
		this.factorCargaMax=factorCargaMax;
		this.capacidad=capacidad;
		arreglo= new NodoHash[capacidad];

	}

	public void put(K llave, V valor){
		int lugar=hash(llave);
		Boolean puesto=false;
		while(!puesto)
		{
			if(arreglo[lugar]==null){
				arreglo[lugar]=new NodoHash<K, V>(llave, valor);
				puesto=true;
			}
			else 
				lugar=(lugar==capacidad-1)?0:lugar++;
			
				
			
		}
		count++;
		factorCarga=count/capacidad;
		if(factorCarga>=factorCargaMax)	
			resize();
		
	}

	public V get(K llave){
		int lugar=hash(llave);
		while(true)
		{
			if(arreglo[lugar]==null)
			{
				return null;
			}
			else if(arreglo[lugar].getLlave().equals(llave))
			{
				return (V) arreglo[lugar].getValor();
			}else
			{
				lugar=(lugar==capacidad-1)?0:lugar++;
			}
			
		}
	}

	public V delete(K llave){
		int lugar=hash(llave);
		while(true)
		{
			if(arreglo[lugar]==null)
			{
				return null;
			}
			else if(arreglo[lugar].getLlave().equals(llave))
			{
				V valor= (V) arreglo[lugar].getValor();
				arreglo[lugar]=null;
				return valor;
			}else
			{
				lugar=(lugar==capacidad-1)?0:lugar++;
			}
			
		}
		
	}

	//Hash
	private int hash(K llave)
	{
		return llave.hashCode()%capacidad;
	}
	//TODO: Permita que la tabla sea dinamica
	private void resize()
	{
		int nuevaCapacidad=capacidad*2+1;
		NodoHash[] nA= new NodoHash[nuevaCapacidad];
		TablaHash nueva= new TablaHash<>(nuevaCapacidad,factorCargaMax);
		for(int i =0;i<capacidad;i++)
		{
			nueva.put((K)arreglo[i].getLlave(), arreglo[i].getValor());
		}
		arreglo=nueva.darArreglo();
		capacidad=nuevaCapacidad;
		factorCarga=nueva.darFactorCarga();
	}
	public NodoHash[] darArreglo()
	{
		return arreglo;
	}
	public Float darFactorCarga()
	{
		return factorCarga;
	}
	protected void set(K llave, V valor)
	{
		int lugar=hash(llave);
		
		if(arreglo[lugar]==null)
		{
			arreglo[lugar]= new NodoHash<K, V>(llave, valor);
			count++;
			factorCarga=count/capacidad;
			if(factorCarga>=factorCargaMax)	
				resize();
		}
		else
		{
			arreglo[lugar].setValor(valor);
		}
	}

}