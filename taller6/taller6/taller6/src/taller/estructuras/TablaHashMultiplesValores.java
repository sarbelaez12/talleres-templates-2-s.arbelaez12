package taller.estructuras;

public class TablaHashMultiplesValores <K extends Comparable<K> ,V>
{
	private TablaHash<K, V[]> tabla;
	public TablaHashMultiplesValores ()
	{
		tabla=new TablaHash<>();
	}
	public TablaHashMultiplesValores (int capacidad, Float cargaMAx)
	{
		tabla=new TablaHash<>(capacidad,cargaMAx);
	}
	public void put(K llave, V valor)
	{
		V[] buscado= (V[]) tabla.get(llave);
		if(buscado==null)
		{
			V[] arreglo = (V[]) new Object[1];
			arreglo[0]= valor;
			tabla.set(llave, arreglo);
			return;
		}
		V[] arregloBuscado= tabla.get(llave);
		V[] arregloNuevo= (V[]) new Object[arregloBuscado.length+1];
		for(int i =0; i < arregloNuevo.length-1;i++)
		{
			arregloNuevo[i]=arregloBuscado[i];
		}
		arregloNuevo[arregloNuevo.length-1]= valor;
		tabla.set(llave, arregloNuevo);
		
	}
	public V[] get(K llave)
	{
		return tabla.get(llave);
	}

}
