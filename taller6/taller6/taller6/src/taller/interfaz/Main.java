package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Mundo.Contacto;
import Mundo.Directorio;


public class Main {

	private static String rutaEntrada = "./data/ciudadcentral.csv";
	static Directorio directorio;
	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio telefonico de Ciudad Central");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio telefonico
			directorio= new Directorio();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				Contacto actual= new Contacto(datos[0], datos[1],Integer.parseInt(datos[2]));
				
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Ciudad Central Directorio v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar un contacto telefónico");
				System.out.println("2: Buscar un contacto telefónico");
				System.out.println("3: Buscar un contacto telefónico por su nombre y/o apellido ");
				System.out.println("Exit: Salir de la aplicación");

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar un contacto telefónico
					System.out.println("Ingrese el nombre del contacto");
					String nombre = br.readLine();
					System.out.println("Ingrese el apellido del contacto");
					String apellido = br.readLine();
					System.out.println("Ingrese el telefono del contacto");
					String telefono = br.readLine();
					directorio.agregarContacto(new Contacto(nombre, apellido, Integer.parseInt(telefono)));
					System.out.println("Se agrego el contacto con exito, espiche Enter para continuar");
					br.readLine();
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un contacto telefonico por su número de teléfono
					System.out.println("Ingrese el telefono del contacto a buscar");
					String telefono1 = br.readLine();
					Contacto buscado= directorio.buscarContactoPorTelefono(Integer.parseInt(telefono1));
					if(buscado==null)
						System.out.println("No se encontro ningun contacto");	
					else
						System.out.println("Contacto buscado: "+buscado.toString());
						System.out.println("Enter para volver al menu principal");
						br.readLine();
					break;
					case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar en la agenda por nombre y/o apellido
						System.out.println("Ingrese el nombre de los contactos a buscar");
						String nombre1 = br.readLine();
						Contacto[] buscado2= directorio.buscarContactoPorApellidoYNombre(nombre1);
						if(buscado2==null)
							System.out.println("No se encontro ningun contacto");	
						else
							for(Contacto actual:buscado2)
							{
								System.out.println(actual.toString());
							}
						System.out.println("Enter para volver al menu principal");
						br.readLine();
						break;
					//TODO: Si desea realizar el bono implemente el case
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
